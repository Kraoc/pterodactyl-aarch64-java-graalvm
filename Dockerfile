# ----------------------------------
# Pterodactyl Core Dockerfile
# Environment: Java
# Minimum Panel Version: 1.7.0
# ----------------------------------
# Copyright (c) 2021 Matthew Penner
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ----------------------------------

# 2022-08-17

FROM    debian:stable-slim

ARG     JAVA_VERSION
ARG     GRAAL_JAVA
ARG     GRAAL_VERSION
ARG     GRAAL_OS
ARG     GRAAL_ARCH
ARG     GRAAL_HOME

CMD     ["bash"]

RUN     set -eux && \
        apt-get update && \
        apt-get install -y --no-install-recommends ca-certificates p11-kit && \
        rm -rf /var/lib/apt/lists/*

ENV     LANG                C.UTF-8
ENV     GRAAL_HOME          ${GRAAL_HOME}
ENV     JAVA_VERSION        ${JAVA_VERSION}
ENV     GRAAL_JAVA          ${GRAAL_JAVA}
ENV     GRAAL_VERSION       ${GRAAL_VERSION}
ENV     GRAAL_OS            ${GRAAL_OS}
ENV     GRAAL_ARCH          ${GRAAL_ARCH}
ENV     JAVA_HOME           ${GRAAL_HOME}
ENV     PATH                ${JAVA_HOME}/bin:${PATH}

COPY    files/graalvm.tgz       /tmp/graalvm.tgz
COPY    files/native-image.jar  /tmp/native-image.jar

RUN     echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN     apt-get update && \
        apt-get install -y --no-install-recommends wget tzdata curl ca-certificates fontconfig locales binutils procps bash libudev1 fonts-dejavu-core && \
        rm -rf /var/lib/apt/lists/* && \
        mkdir -p "${GRAAL_HOME}" && \
        tar --extract --file /tmp/graalvm.tgz --directory ${GRAAL_HOME} --strip-components 1 --no-same-owner && \
        rm -rf /tmp/graalvm.tgz

LABEL   author              "Olivier Le Bris"
LABEL   maintainer          "cybridenet@gmail.com"
LABEL   com.centurylinklabs.watchtower.enable=false

LABEL   org.opencontainers.image.source     "https://zogg.ddns.net"
LABEL   org.opencontainers.image.licenses   MIT

RUN     apt-get update -y && \
        apt-get install -y curl ca-certificates openssl git tar sqlite3 fontconfig libfreetype6 tzdata iproute2 libstdc++6 bash && \
        useradd -d /home/container -m container && \
        gu -L install /tmp/native-image.jar && \
        rm -rf /tmp/native-image.jar

USER    container
ENV     USER                container
ENV     HOME                /home/container
WORKDIR /home/container

ENV     LANG                C.UTF-8
ENV     GRAAL_HOME          ${GRAAL_HOME}
ENV     JAVA_VERSION        ${JAVA_VERSION}
ENV     GRAAL_JAVA          ${GRAAL_JAVA}
ENV     GRAAL_VERSION       ${GRAAL_VERSION}
ENV     GRAAL_OS            ${GRAAL_OS}
ENV     GRAAL_ARCH          ${GRAAL_ARCH}
ENV     JAVA_HOME           ${GRAAL_HOME}
ENV     PATH                ${JAVA_HOME}/bin:${PATH}

COPY    files/docker-java-home  /usr/local/bin/docker-java-home
COPY    files/bashrc            /home/container/.bashrc
COPY    files/entrypoint.sh     /entrypoint.sh

CMD     ["/bin/bash", "/entrypoint.sh"]
